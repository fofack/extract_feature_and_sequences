
#!bin/bash
#echo -e "Enter organism abreviation:\n"
#read name
mkdir organism
mkdir kegg
mkdir kegg/id
wget -nc https://rest.kegg.jp/list/$1 --output-document=kegg/$1.txt

#extraction of gens numbers

cut -f 1 kegg/$1.txt >> kegg/id/$1.txt
mkdir organism/$1

while read line 
do gene=$(wget -qO- https://rest.kegg.jp/get/$line/NTSEQ)
   protein=$(wget -qO- https://rest.kegg.jp/get/$line/AASEQ)
   echo "$gene" >> organism/$1/DNA_Sequence.fasta
   echo "$protein" >> organism/$1/protein_Sequence.fasta	
done < kegg/id/$1.txt
